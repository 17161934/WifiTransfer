package net.muliba.wifitransfer

/**
 * Created by fancyLou on 2018/1/22.
 * Copyright © 2018 O2. All rights reserved.
 */
data class FileData(var name: String = "", var type: String = "")

data class FileResponse(var path: String = "", var list: List<FileData> = ArrayList())