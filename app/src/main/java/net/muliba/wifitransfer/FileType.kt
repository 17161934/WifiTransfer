package net.muliba.wifitransfer

/**
 * Created by fancyLou on 2018/1/22.
 * Copyright © 2018 O2. All rights reserved.
 */

enum class FileType(val type:String="") {
    folder("folder"),
    img("img"),
    video("video"),
    file("file");
}